The folder has a few simple python scripts that are to be expanded for more functionality later on.  

RC522 connected with Raspberry Pi using the following layout:  

| Name | Pin # | Pin name   |
|:------:|:-------:|:------------:|
| SDA  | 24    | GPIO8      |
| SCK  | 23    | GPIO11     |
| MOSI | 19    | GPIO10     |
| MISO | 21    | GPIO9      |
| IRQ  | None  | None       |
| GND  | Any   | Any Ground |
| RST  | 22    | GPIO25     |
| 3.3V | 1     | 3V3        |


Raspberry Pi requirements:  

sudo echo “dtparam=spi=on“ >> /boot/config.txt  
sudo apt-get install python3-dev python3-pip  
sudo pip3 install spidev  
sudo pip3 install mfrc522  





Source, more information at:  
https://pimylifeup.com/raspberry-pi-rfid-rc522/
