#!/usr/bin/env python
import RPi.GPIO as GPIO
import time
from mfrc522 import SimpleMFRC522

GPIO.setmode(GPIO.BOARD)

redled = 32
greenled = 31

GPIO.setup(redled,GPIO.OUT)
GPIO.setup(greenled,GPIO.OUT)

reader = SimpleMFRC522()

try:
	while True:
		id, text = reader.read()
		if id == 730019095477:
			GPIO.output(greenled,GPIO.HIGH)
			time.sleep(0.1)
			GPIO.output(greenled,GPIO.LOW)
			time.sleep(0.1)
			GPIO.output(greenled,GPIO.HIGH)
			time.sleep(0.1)
			GPIO.output(greenled,GPIO.LOW)
		else:
			GPIO.output(redled,GPIO.HIGH)
			time.sleep(0.1)
			GPIO.output(redled,GPIO.LOW)
			time.sleep(0.1)
			GPIO.output(redled,GPIO.HIGH)
			time.sleep(0.1)
			GPIO.output(redled,GPIO.LOW)

except KeyboardInterrupt:
	GPIO.cleanup()
