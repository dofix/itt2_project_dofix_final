---
Week: 18
Content: Project part 2 phase 1
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 18 - tentative

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
None from the teachers at this time.


### Learning goals
None from the teachers at this time.

## Deliverables
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)

## Schedule

Monday

* 8:15 Introduction to the day, general Q/A session

* 9:30? You work

    Group morning meeting
    
        You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
        Ordinary agenda:
        1. (5 min) Round the table: What did I do, and what did I finish?
        2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
        3. (5 min) Round the table: Claim one task each.

    Remember to come ask questions if you have any.  

* 10:30 Experience exchange

    See below for details

Tuesday

* 8:15 Introdution to the day, general Q/A session

* 8:30 Group morning meeting (See monday for agenda)

* 9:00 Teacher meetings

    Timeslot for you weekly 10 min. meeting with the teachers.

    Remember to book a time and have an agenda prepared.

* 9:00 You work

    Come ask us if you have questions.

## Hands-on time

### Experience exchange

The purpose is for you to revise your way of doing team work and manage your project.

The format

* Each member brings a good and a bad example from his own work
* Each member will present his example (1-2 min) and if applicable, the group will ask clarifying questions and/or come with solution suggestion (5 min max)
    * This means that not everybody will hae a chance of presenting their examples.
* Examples are from the following broad topics
    * Using gitlab
    * Defining goals and task
    * Day-to-day operations
    * Documentation
    * Resource allocation and use (time, money, ...)
* We will not make it mandatory to speak in the forum
* One of the teachers will do timekeeping, and we will stop after 60 minutes.




See the [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details.


## Comments

* The ESD course was planned for this week. It has been postponed.