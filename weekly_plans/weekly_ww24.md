---
Week: 24
Content: Project part 2 phase 3
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 24 - tentative

This is the last project week

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
None from the teachers at this time.

### Learning goals
None from the teachers at this time.

## Deliverables
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)

## Schedule

Monday

* Whit Monday - no lectures planned

Tuesday

* 8:15 Introduction to the day, general Q/A session
    - Q&A list is on [hackmd.io](https://hackmd.io/29oDMYmEQ5i1hfsrL1D98g)

* 9:45? Demoday preparation and setup

* 10:00 Demoday

    Demonstration of the project part 2 result.  

* 12:15 Q&A session

    This is a session where we answer questions regarding the project and exam recap questions.

    We will use the Q&A list on [hackmd.io](https://hackmd.io/29oDMYmEQ5i1hfsrL1D98g) to gather questions and answers

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details, if applicable.


## Comments

we only have one day this week because of Whit Monday