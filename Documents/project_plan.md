### Table of Contents ###
----

1. Backgrounds
2. Purpose
3. Goals
4. Schedule
5. Organization
6. Budget and resources
7. Risk assessment 
8. Stakeholders
9. Communication
10. Perspectives
11. Evaluation
12. References



### 1. Backgrounds ###  
----
This is the semester project for ITT2 where we will combine different 
technologies. This project will cover the second (final) part of the 
project, and will match topics that are taught in parallel classes.  

### 2. Purpose ###   
----
The purpose of this project is to create the system for the end user. 
The system will control a padlock from a website. The padlock can be 
also opened and closed with a physical key. The requirements will be specified in Evaluation section.

### 3. Goals ###  
----
The overall system that is going to be build looks as follows  
Reading from the top left to bottom right  
![project_overview](project_overview.png)  
* Electronic padlock:
    * RFID reader - reading the keys using RFID protocol
    * ESP8266-12E - Wifi module, runs micropython program to communicate across the network and controls the stepper motor in the lock
* Arduino Uno R3 - used to program ESP8266-12E
* PC - used to program RFID chips through the RFID writer and serial-connect to Arduino Uno R3
* Blade server:
    * webserver- serving as an authorization and administration alternative to open the electronics padlock without RFID chip
    * SQ lite database - dataloging and RFID chips information 

Project deliveries are:
* TBA

The project itself will be divided into 3 phases and gitlab will be used as the project management platform.

### 4. Schedule ###
----  
#### Description
The schedule provides time related information in the semester project. The project will end in week 24 along with the semester. The essential is the amount of working days for the project in each stage. The information is specified only for one stage ahead at the time. The second stage schedule will be specified after the completion of stage one.

##### The second stage
The second stage lasts **8 weeks**(week 17 - week 24). The working days are **Monday** and **Tuesday** with minor changes announced for particular week.
The time frame within a regular working day is 7 hours with lunch break. The extra hours of work might be required when the team runs into conditions described in the Risk assessment section of this document.

###### Monday schedule
* 8:15 - 9:00 Teacher introduction to the day
* 9:00 - 10:30 Issues work time
* 10:30 - 11:30 Class meeting
* Lunch 
* 12:15 - 13:00 Claim issues
* 13:00 - 15:30 Issues work time

###### Tuesday schedule
* 8:15 - 9:00 Teacher introduction to the day
* 9:00 - 11:30 Issues work time/ lecture & group meetings 
* Lunch
* 12:15 - 14:30 Group discussion
* 14:30 - 15:30 Week summary and planning

Further information will be added as we progress.
### 5. Organization ###
----  
##### Description
The organization of a project usually consists of three main elements. 
Levente Atilla Taner is the project manager. Andrej Královič is the networking manager and Nicklas Tange Præstegaard is the electronics manager.The group members work 
together as a team with similar responsibilities and report to each 
other and to the teachers which fullfill the role of a project board. 
Thus, the project board would be teachers responsible for the 2nd semester project work.

##### The group members:
  
* Andrej Královič  
* Levente Atilla Taner  
* Nicklas Tange Præstegaard

##### The project board:

* Nicolaj Simonsen
* Morten Nielsen 

### 6. Budget and resources ###
----
Resources required to finish the project were ordered and will be 
implemented in the final product: 
* ESP8266-12E......77,84 dkk
* 3x RC 522 kit....78,03 dkk 
 
In terms of manpower, only the people in the project group are expected to contribute.

### 7. Risk assessment ###
----  
The Risk assessment looks at possible risks that could lead to the failure of the project and takes possible solutions into consideration.

### Risks:

- New and unknown communication platforms between lecturers and the class  
- New project with goals
- Lack of motivation related to summer break approaching quickly  
- Too many tasks at once  
- Time management    
- Uncertainties

### Possible solutions:  

   
**Knowledge**

- Seek knowledge / help from classmates.
- Be honest with the goals, never hide your progress! 
- Take the time to explore unknown and new platforms and goals.



**Planning**

- Make sure to keep group meetings, a regular thing. It is important for everyone to be on the same page.
- Never leave issues unresolved!
- Make priorities when planning.
- Make schedules, clear tasks - SMART tasks

**Motivation**

- Take short breaks, walk around and get fresh air.
- Don't overwork yourself in one sitting.  
### 8. Stakeholders ###
----  
The Stakeholders section includes information about stakeholders who are affected by and actively or passively influence the process and outcome of the project.
  
**End user:**  
- External stakeholders  
- Positive stakeholders  

**Group members:**  
- Internal stakeholders  
- Positive stakeholders  
  
**The project board:**   
- Internal stakeholders  
- Positive stakeholders  

The individual and parties that are the part of the organization is known as Internal Stakeholders. The parties or groups that are not a part of the organization, but gets affected by its activities is known as External Stakeholders.

Negative stakeholders will be negatively affected by the project's success, while positive stakeholder will be favorably (positively) affected by the project's success.


From project stand-point, the End user has priority over other stakeholder groups but from a technical point the project board has priority over the other stakeholder groups.


### 9. Communication ###
----  
Communication is one of the key elements for a project to hold up. Without proper communication between the stakeholders, the project will not end with the right outcome, or maybe with the right outcome but not on time.

**Communication strategy**
*  Team meeting every tuesday
*  Constant information about independant stakeholder work
*  Gitlab issues has to be approved by anyone in the group before they're closed. This is done by "liking" the comment on the issue. Once this is done by all team member, the issue can be closed.

**Communication platforms**
*  Slack (Primary)
*  Facebook (Secondary)
*  Mail (Tertiary)


**Contact info**
<table>
    <tr>
        <th>GitLab username</th>
        <th>Name</th>
        <th>E-mail</th>
    </tr>
    <tr>
        <td>jambove</td>
        <td>Levente Atilla Taner</td>
        <td>leve0147@edu.eal.dk</td>
    </tr>
    <tr>
        <td>ak98</td>
        <td>Andrej Královič</td>
        <td>andr44a1@edu.eal.dk</td>
    </tr>
    <tr>
        <td>nick3359</td>
        <td>Nicklas Tange Præstegaard</td>
        <td>nick3359@edu.eal.dk</td>
</table>

### 10. Perspectives
----  
#### Description
The Perspectives section contains information on who and how will benefit in relation to the project.

####  Group members  
  
The group members will learn how to ooperate within the group. The communication skills are very important in real life working environment. The GitLab platform will be used as a communication channel and project managment tool. The team will acquire knowledge working in team and using GitLab to manage it. The team will implement the knowledge from the other classes(programing in C, electronics and network) and will get familiar with hardware(Raspberry Pi,Juniper SRX routers,ATMega 328 Mini Xplained).

#### Lecturers  
  
The lecturers will be able to benefit from the project also. The knowledge acquired by students and vice versa can be shared. The lecturers will receive feedback, which will enable them to improve in the future.

#### Employers  
  
The employers will be able to employ the students, who have possesed the knowledge in programing, electronics, network and soft skills regarding the team work.

#### Users  
  
The users can take advantage of the system produced within the project with application to different areas of everyday life.

#### Institution  
  
The UCL University College Denmark will be recognised as a higher educational institution which is offering quality education and helps its' students as much as possible to get decent education and fulfilling job in the future.

### 11. Evaluation ###
----  
The evaluation part of the project is an insurance for a positive end product.

**First state evaluation**
*  Test the product on our own.
*  Evaluate teamwork
*  Evaluate process
*  Fix errors and problems

**Second state evaluation**
*  Make third party testpersons, test the product to find errors before launching the product.
*  Evaluate teamwork
*  Evaluate process
*  Fix errors and problems

**End state evaluation**
*  Make third party testperson, test the product. This state is the last possible time, to fix any errors.
*  Evaluate the project

### 12. References ###
Files from the lecturers at https://eal-itt.gitlab.io/19s-itt2-project/
