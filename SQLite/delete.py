import sqlite3

def create_connection(db_file):
	""" create a database connection to a SQLite database """
	try:
		conn = sqlite3.connect(db_file)
		return conn
	except Error as e:
		print(e)
	return None
	
def delete_task(conn, id):
	"""
	Delete an rfid_chips by rfid_chips id
	:param conn:  Connection to the SQLite database
	:param id: id of the rfid_chips
	:return:
	"""
	sql = 'DELETE FROM rfid_chips WHERE id=?'
	cur = conn.cursor()
	cur.execute(sql, (id,))

def main():
	database = "padlock.db"
 
	# create a database connection
	conn = create_connection(database)
	with conn:
		delete_task(conn, 730019095476);
		#delete_all_tasks(conn);
 
 
if __name__ == '__main__':
	main()
