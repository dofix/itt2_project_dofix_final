import sqlite3

def create_connection(db_file):
	""" create a database connection to a SQLite database """
	try:
		conn = sqlite3.connect(db_file)
		return conn
	except Error as e:
		print(e)
	return None
	
def update_rfid(conn, rfid):
	"""
	update id, uid, and register_date of a rfid_chips
	:param conn:
	:param task:
	:return: rfid_chips id
	"""
	sql = ''' UPDATE rfid_chips
				SET id = ? ,
					uid = ? ,
					register_date = ?
				WHERE id = ?'''
	cur = conn.cursor()
	cur.execute(sql, rfid)

def main():
	database = "padlock.db"
 
	# create a database connection
	conn = create_connection(database)
	with conn:
		update_rfid(conn, (73001909549, 'A9F88763B7', '2019-06-06',730019095478))
 
if __name__ == '__main__':
	main()
