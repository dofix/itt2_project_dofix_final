import sqlite3

def create_connection(db_file):
	""" create a database connection to a SQLite database """
	try:
		conn = sqlite3.connect(db_file)
		return conn
	except Error as e:
		print(e)

	return None

def create_table(conn, create_table_sql):
	""" create a table from the create_table_sql statement
	:param conn: Connection object
	:param create_table_sql: a CREATE TABLE statement
	:return: """
	try:
		c = conn.cursor()
		c.execute(create_table_sql)
	except Error as e:
		print(e)

def main():
	database = "../sqlite/padlock.db"

	sql_create_rfid_chips_table = 	""" CREATE TABLE IF NOT EXISTS rfid_chips (
					id INTEGER PRIMARY KEY,
					uid VARCHAR(32) NOT NULL,
					register_date date
					); """

	sql_create_padlocks_table = 	""" CREATE TABLE IF NOT EXISTS padlocks (
					id INTEGER PRIMARY KEY,
					serial_nr VARCHAR(32) NOT NULL
					); """

	sql_create_padlock_rfid_table = """ CREATE TABLE IF NOT EXISTS padlock_rfid (
					rfid_id INTEGER,
					padlock_id INTEGER,
					FOREIGN KEY(rfid_id) REFERENCES rfid_chips(id),
					FOREIGN KEY(padlock_id) REFERENCES padlocks(id)
					); """

	# create a database connection
	conn = create_connection(database)
	if conn is not None:
		# create rfid_chips table
		create_table(conn, sql_create_rfid_chips_table)
		# create padlocks table
		create_table(conn, sql_create_padlocks_table)
		# create padlock_rfid relation table
		create_table(conn, sql_create_padlock_rfid_table)
	else:
		print("Error! cannot create the database connection.")

if __name__ == "__main__":
	main()
