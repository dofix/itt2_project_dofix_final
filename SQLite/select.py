import sqlite3
 
 
def create_connection(db_file):
	""" create a database connection to the SQLite database
		specified by the db_file
	:param db_file: database file
	:return: Connection object or None
	"""
	try:
		conn = sqlite3.connect(db_file)
		return conn
	except Error as e:
		print(e)
 
	return None
 
 
def select_all_rfid_chips(conn):
	"""
	Query all rows in the rfid_chips table
	:param conn: the Connection object
	:return:
	"""
	cur = conn.cursor()
	cur.execute("SELECT * FROM rfid_chips")
 
	rows = cur.fetchall()
 
	for row in rows:
		print(row)
 
 
def select_rfid_chips_by_register_date(conn, register_date):
	"""
	Query rfid_chips by register_date
	:param conn: the Connection object
	:param register_date:
	:return:
	"""
	cur = conn.cursor()
	cur.execute("SELECT * FROM rfid_chips WHERE register_date=?", (register_date,))
 
	rows = cur.fetchall()
 
	for row in rows:
		print(row)
 
 
def main():
	database = "padlock.db"
 
	# create a database connection
	conn = create_connection(database)
	with conn:
		#print("Query task by register_date:")
		#select_rfid_chips_by_register_date(conn,1) dates need to be in other format
 
		print("Query all tasks")
		select_all_rfid_chips(conn)
 
 
if __name__ == '__main__':
	main()
