import sqlite3

def create_connection(db_file):
	""" create a database connection to a SQLite database """
	try:
		conn = sqlite3.connect(db_file)
		return conn
	except Error as e:
		print(e)
	return None

def create_rfidentry(conn, rfidentry):
	"""
	Create a new rfidentry
	:param conn:
	:param rfidentry:
	:return:
	"""

	sql = ''' INSERT INTO rfid_chips(id,uid,register_date)
		VALUES(?,?,?) '''
	cur = conn.cursor()
	cur.execute(sql, rfidentry)
	return cur.lastrowid

def main():
	database = "./padlock.db"

	# create a database connection
	conn = create_connection(database)
	with conn:
		# create a new project
		#project = ('Cool App with SQLite & Python', '2015-01-01', '2015-01-30');
		#project_id = create_project(conn, project)

		# tasks
		rfidentry_1 = ('730019095476', 'A9F88763B4', '2019-06-04')
		rfidentry_2 = ('730019095478', 'A9F88763B6', '2019-06-04')
 
		# create tasks
		create_rfidentry(conn, rfidentry_1)
		create_rfidentry(conn, rfidentry_2)

if __name__ == '__main__':
	main()
