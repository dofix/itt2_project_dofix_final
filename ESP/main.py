def PWM_setup(x = "pwm14"):
    from machine import Pin,PWM
    x = PWM(Pin(int(x[3:5])))
    x.freq(50)
    #print(x.freq())
    return x

pwm14 = PWM_setup()
#print(pwm14.freq())
#print(pwm14.duty())

def open_lock(x = pwm14):
    x.duty(103)
    print("..... Opening the lock.....")
    #print(x.duty())
    return True

def close_lock(x = pwm14):
    x.duty(67)
    print("..... Closing the lock.....")
    #print(x.duty())
    return False

def pwm_off(x = pwm14):
    x.deinit()
