import mfrc522
from os import uname
import main

approved= [35, 202, 211, 27, 33]

def do_read():
        status = False
        
	if uname()[0] == 'WiPy':
		rdr = mfrc522.MFRC522("GP14", "GP16", "GP15", "GP22", "GP17")
	elif uname()[0] == 'esp8266':
		rdr = mfrc522.MFRC522(4, 16, 12, 5, 2)
	else:
		raise RuntimeError("Unsupported platform")

	print("")
	print("Place card before reader to read from address 0x08")
	print("")

	try:
		while True:
			(stat, tag_type) = rdr.request(rdr.REQIDL)

			if stat == rdr.OK:

				(stat, raw_uid) = rdr.anticoll()

				if stat == rdr.OK:
					print("New card detected")
					print("  - tag type: 0x%02x" % tag_type)
					print("  - uid	 : 0x%02x%02x%02x%02x" % (raw_uid[0], raw_uid[1], raw_uid[2], raw_uid[3]))
					print("")
					print(raw_uid)

					if rdr.select_tag(raw_uid) == rdr.OK:

						key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]

						if rdr.auth(rdr.AUTHENT1A, 8, key, raw_uid) == rdr.OK:
							print("Address 8 data: %s" % rdr.read(8))
							rdr.stop_crypto1()
							if raw_uid == approved:
                                                                print("Authentication OK!")
                                                                if not status:
                                                                        status = main.open_lock()
                                                                else:
                                                                        status = main.close_lock()
                                                        else:
                                                                print("You are not authorized to open this lock!")
                                                                print("  - uid	 : 0d%02d%02d%02d%02d%02d" % (raw_uid[0], raw_uid[1], raw_uid[2], raw_uid[3], raw_uid[4]))
                                                                print("  - uid	 : 0d%02d%02d%02d%02d%02d" % (approved[0], approved[1], approved[2], approved[3], approved[4]))
						else:
							print("Authentication error")
					else:
						print("Failed to select tag")

	except KeyboardInterrupt:
		print("Bye")
